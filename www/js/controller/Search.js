angular.module('search.controllers', ['ionic', 'ngCordova'])

.controller('SearchCtrl', function($scope, $stateParams, $ionicModal, $timeout, $http, $ionicLoading) {
   function ucfirst(str,force){
        str=force ? str.toLowerCase() : str;
        return str.replace(/(\b)([a-zA-Z])/,
                 function(firstLetter){
                    return   firstLetter.toUpperCase();
                 });
   }
  $('input[type="search"]').keyup(function(evt){

      // force: true to lower case all letter except first
      var cp_value= ucfirst($(this).val(),true);
      $(this).val(cp_value );

   });
    $scope.search = function(query) {
      $ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});
      var req = {
          method: 'GET', 
          url: baseUrl+'api/search?query='+query,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      };
      if (query !== null) {
        $http(req).success(function(data) { 
            console.log(data);
            $scope.products = data.products;
            $ionicLoading.hide(); 
        }).error(function(data) { 
            $ionicLoading.hide(); 
        });
      }
    }
    $scope.clearSearch = function() {
      $('input[type="search"]').val(null);
    }

 });