angular.module('profile.controllers', ['ionic', 'ngCordova'])

.controller('ProfileCtrl', function($scope, $stateParams, $ionicModal, $timeout, $http, $ionicLoading) {

  $ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});
  $scope.email = window.localStorage['email'];
    var req = {
        method: 'GET', 
        url: baseUrl+'api/customer/profile?email='+$scope.email,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    };
    $http(req).success(function(data) { 
    	  console.log(data);
        $scope.orders = data.orders.items;
        $scope.address = data.address;
        $scope.user = data.user;
        $ionicLoading.hide();
    }).error(function(data) { 
        $ionicLoading.hide(); 
    });

    $scope.sortByOrderStatus = function(orderStatus) {
      $scope.sortedOrders = [];
      $.each($scope.orders, function(index, el) {
        if (el.status === orderStatus) {
          $scope.sortedOrders.push(el);
        }
        if (orderStatus === 'all') {
          $scope.sortedOrders.push(el);
        }
      });
    }

 });