angular.module('checkout.controllers', ['ionic', 'ngCordova'])

.controller('CheckoutCtrl', function($scope, $stateParams, $ionicModal, $timeout, $http, $ionicLoading, $cordovaToast) {

    $scope.doRefresh = function(){
        location.reload();
    }

  $ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});
  $scope.cartId = window.localStorage['cart_id'];

  var req = {
        method: 'GET', 
        url: baseUrl+'api/checkout/cart?cart_id='+$scope.cartId,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    };
    $http(req).success(function(data) { 
    	console.log(data);
        $scope.quote = data.quote;
        $scope.quoteItems = data.quote_items;
        $ionicLoading.hide();
    }).error(function(data) { 
        $ionicLoading.hide(); 
    });

    $scope.createOrder =  function() {
        $ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});
        var req = {
            method: 'POST', 
            url: baseUrl +'api/sales/order', 
            data: $.param({cart_id: $scope.cartId}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        };

        $http(req).success(function(data) {
            if(data.status == 'success'){
                window.localStorage['cart_id'] = '';
                $scope.orderNumber = data.order_increment_id;
                window.location = '#/app/success/'+data.order_increment_id;
                $ionicLoading.hide();
            }else{
                $ionicLoading.hide();
            }
        }).error(function(data) {});
    }
 });

angular.module('success.controllers', ['ionic', 'ngCordova'])
.controller('SuccessCtrl', function($scope, $stateParams, $ionicModal, $timeout, $http, $ionicLoading, $cordovaToast) {
    $scope.orderNumber = $stateParams.order_increment_id;
});


