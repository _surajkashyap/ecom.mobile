angular.module('productdetail.controllers', ['ionic', 'ngCordova', 'ionic-toast'])

.controller('ProductDetailCtrl', function($ionicHistory, $scope, $stateParams, $ionicModal, $timeout, $http, $ionicLoading, $ionicPopup, $cordovaToast, ionicToast) {
	
	var param = $stateParams.productId;
	$ionicHistory.clearCache();
	$scope.userProductData = {};
	$scope.userProductData.qty = "1";
	$ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});

	$scope.showAlert = function(title, message) {
	   var alertPopup = $ionicPopup.alert({
	     title: title,
	     template: message,
	     buttons: [{
	        text: 'Alright',
	        type: 'button-cyan'
	      }] 
	   });
	   alertPopup.then(function(res) {
	   });
	};

	$('.minus').click(function(event) {
		var currentVal = $('.qty').val();
		var finalqty = parseFloat(currentVal) - 1
		if (finalqty >= 1) {
			$('.qty').val(finalqty);
			$scope.userProductData.qty = finalqty;
		}
		
	});

	$('.plus').click(function(event) {
		var currentVal = $('.qty').val();
		var finalqty = parseFloat(currentVal) + 1
		$('.qty').val(finalqty)
		$scope.userProductData.qty = finalqty;
	});

	function loadDetailBySKUAction() {
		var req = {
	        method: 'GET', 
	        url: baseUrl+'api/rest/products/'+param,
	        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    };
	    $http(req).success(function(data) { 
	    	console.log(data);
	        $scope.product = data;
	        $scope.product_id = data.entity_id;
	        $scope.oldPrice = parseFloat(data.final_price_with_tax)+8;
	        $ionicLoading.hide(); 
	    }).error(function(data) { 
	        $ionicLoading.hide(); 
	    });
	}
	$scope.addToCart = function(qty) {
		var cartId = window.localStorage['cart_id'];
		var productId = $scope.product_id;
		var email = window.localStorage['email'];
		var qty = $scope.userProductData.qty;
		if (!email) {
			$scope.login();
			ionicToast.show('Please login before adding '+$scope.product.name+' to you cart :(', 'bottom', false, 2500); 
		}
		if (email) {
			$ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});
			var req = {
		        method: 'POST', 
		        url: baseUrl +'api/product/addToCart', 
		        data: $.param({email : email, product_id : productId, qty : qty, cart_id : cartId}),
		        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    	};

		    $http(req).success(function(data) {
		        if(data.status == 'success'){
		        	window.localStorage['cart_id'] = data.cart_id;
		            $ionicLoading.hide();
		            console.log(data);
		            ionicToast.show('Added '+$scope.product.name+' to you cart.', 'bottom', false, 2500); 
		        }else{
		        	$scope.showAlert('Error', data.message);
		            $ionicLoading.hide();
		        }
		    }).error(function(data){});
		}
	}

	loadDetailBySKUAction();
});