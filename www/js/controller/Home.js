angular.module('starter.controllers', ['ionic', 'ngCordova'])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $ionicLoading, $http, $ionicPopup, ionicToast, $rootScope, $ionicUser, $ionicPush, $cordovaLocalNotification) {
    $scope.email = window.localStorage['email'];

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    // Form data for the login modal
    $scope.device_token = window.localStorage['device_token'];
    $scope.user_id = window.localStorage['userId'];
    $scope.loginData = {device_token : $scope.device_token};
    $scope.signUpData = {device_token : $scope.device_token};


    // An alert dialog
    $scope.showAlert = function(title, message) {
        var alertPopup = $ionicPopup.alert({
            title: title,
            template: message,
            buttons: [{
                text: 'Alright',
                type: 'button-cyan'
            }]
        });
        alertPopup.then(function(res) {});
    };

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    $ionicModal.fromTemplateUrl('templates/search.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
        $scope.modal.hide();
    };

    $scope.goTo = function(url) {
        window.location = url;
    }

    $scope.toggleForm = function(hide, show) {
        $('.' + hide).hide();
        $('.' + show).show();
    }

    // Open the login modal
    $scope.login = function() {
        $scope.modal.show();
    };

    $scope.search = function() {
        $scope.modal.show();
    }

    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
        $ionicLoading.show({
            template: '<ion-spinner icon="lines" class="light"></ion-spinner>'
        });
        var req = {
            method: 'POST',
            url: baseUrl + 'api/customer/authenticate',
            data: $.param($scope.loginData),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };

        $http(req).success(function(data) {
            if (data.status == 'success') {
                window.localStorage['email'] = data.token;
                window.localStorage['isUserLoggedIn'] = 1;
                $ionicLoading.hide();
                $scope.modal.hide();
                ionicToast.show('Awesome! You\'ve logged in, enjoy our products exclusively for you :)', 'bottom', false, 3000);
            } else {
                $('.message').text(data.message);
                $('.hidden-messge').focus();
                $('.message').show();
                $ionicLoading.hide();
            }
        }).error(function(data) {

        });
        $('.message').hide();
    };

    $scope.doSignup = function() {
        $ionicLoading.show({
            template: '<ion-spinner icon="lines" class="light"></ion-spinner>'
        });
        console.log($scope.signUpData);
        var req = {
            method: 'POST',
            url: baseUrl + 'api/customer/register',
            data: $.param($scope.signUpData),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };

        $http(req).success(function(data) {
            if (data.status == 'success') {
                $ionicLoading.hide();
                $scope.showAlert('Success', 'Thank you for registering with our store, please login.');
                $scope.toggleForm('register-form', 'login-form');
            } else {
                $('.message').text(data.message);
                $('.message').show();
            }
            $ionicLoading.hide();
        }).error(function(data) {

        });
        $('.message').hide();
    };
})


.controller('DashboardCtrl', ['$scope', 'DeligateDataService', '$http', '$ionicLoading', '$ionicPopover', '$cordovaContacts', '$ionicPopup', function($scope, DeligateDataService, $http, $ionicLoading, $ionicPopover, $cordovaContacts, $ionicPopup) {

}])

.controller('FavoritesCtrl', function($scope) {

});