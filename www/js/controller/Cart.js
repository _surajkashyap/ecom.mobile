angular.module('cart.controllers', ['ionic', 'ngCordova'])

.controller('CartCtrl', function($ionicHistory, $state, $scope, $stateParams, $ionicModal, $timeout, $http, $ionicLoading, ionicToast, $ionicPopup) {
    $ionicHistory.clearCache();
    $scope.doRefresh = function(){
        location.reload();
    }

    $ionicLoading.show({
        template: '<ion-spinner icon="lines" class="light"></ion-spinner>'
    });
    $scope.cartId = window.localStorage['cart_id'];

    var req = {
        method: 'GET',
        url: baseUrl + 'api/checkout/cart?cart_id=' + $scope.cartId,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    };
    $http(req).success(function(data) {
        console.log(data);
        $scope.quote = data.quote;
        $scope.quoteItems = data.quote_items;
        $ionicLoading.hide();
    }).error(function(data) {
        $ionicLoading.hide();
    });

    $scope.removeItem = function(itemId) {
        $ionicLoading.show({
            template: '<ion-spinner icon="lines" class="light"></ion-spinner>'
        });
        var req = {
            method: 'POST',
            url: baseUrl + 'api/product/removeItem',
            data: $.param({
                cart_id: $scope.cartId,
                item_id: itemId
            }),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };

        $http(req).success(function(data) {
            if (data.status == 'success') {
                $ionicLoading.hide();
                ionicToast.show('Removed item from the cart.', 'bottom', false, 2500);
                location.reload();
            } else {
                $ionicLoading.hide();
            }
        }).error(function(data) {});
    }

    $scope.updateQty = function updateQty(itemId, qty) {
        $ionicLoading.show({
            template: '<ion-spinner icon="lines" class="light"></ion-spinner>'
        });

        var req = {
            method: 'POST',
            url: baseUrl + 'api/product/updateItem',
            data: $.param({
                cart_id: $scope.cartId,
                item_id: itemId,
                qty: qty
            }),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };

        $http(req).success(function(data) {
            if (data.status == 'success') {
                $ionicLoading.hide();
                ionicToast.show('Updated your shopping cart.', 'bottom', false, 2500);
                location.reload();
            } else {
                $ionicLoading.hide();
            }
        }).error(function(data) {});  
    }

    $scope.updateCart = function(itemId) {
        $scope.data = {}
        var myPopup = $ionicPopup.show({
            template: '<input type="number" class="input-padding" ng-model="data.qty" min="1" max="100">',
            title: 'Update Your Quantity',
            subTitle: '',
            scope: $scope,
            buttons: [{
                text: 'Cancel'
            }, {
                text: '<b>Update</b>',
                type: 'button-cyan',
                onTap: function(e) {
                    if (!$scope.data.qty || $scope.data.qty == '0') {
                        e.preventDefault();
                    } else {
                        $scope.updateQty(itemId, $scope.data.qty);
                    }
                }
            }]
        });
    }
});