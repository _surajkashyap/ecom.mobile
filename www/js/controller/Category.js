angular.module('category.controllers', ['ionic', 'ngCordova'])

.controller('CategoryCtrl', function($scope, $stateParams, $ionicModal, $timeout, $http, $ionicLoading) {
  $scope.isUserLoggedIn = window.localStorage['isUserLoggedIn'];
  $ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});

  var req = {
      method: 'GET', 
      url: baseUrl+'api/category/list',
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  };

  $http(req).success(function(data) { 
      $scope.categories = data.categories;
      console.log($scope.categories);
      $ionicLoading.hide(); 
  }).error(function(data) { 
      $ionicLoading.hide(); 
  });

  $scope.logout = function() {
    window.localStorage['isUserLoggedIn'] = 0;
    window.localStorage['email'] = '';
    location.reload();
  }
  
  /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };
  
});