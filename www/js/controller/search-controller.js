angular.module('deligates.controllers', ['ionic', 'ngCordova'])

.factory('DeligateDataService', function($q, $timeout) {

  var searchDeligates = function(searchFilter) {

    console.log('Searching deligates for ' + searchFilter);
    
    var deferred = $q.defer();
    var deligates = $.parseJSON(window.localStorage['deligates']);
    var matches = deligates.filter(function(deligate) {
      var fullname = deligate.firstName+' '+deligate.lastName;
      if (deligate.firstName.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1) return true;
      //if (deligate.lastName.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1) return true;
      if (deligate.company.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1) return true;
      if (fullname.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1) return true;
    })

    $timeout(function() {
      deferred.resolve(matches);
    }, 100);
    return deferred.promise;
  };

  return {
    searchDeligates: searchDeligates
  }
})

.controller('DeligatesCtrl', ['$scope', 'DeligateDataService', '$http', '$ionicLoading' , '$ionicPopover', '$cordovaContacts', '$ionicPopup', function($scope, DeligateDataService, $http, $ionicLoading, $ionicPopover, $cordovaContacts, $ionicPopup) {

    function loadData() {
        var req = {
            method: 'GET', 
            url: baseUrl+'api/v1/deligates',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        };
        $ionicLoading.show();
        $http(req).success(function(data) { 
            $scope.deligates = data.deligates;
            console.log(data.deligates);
            window.localStorage['deligates'] = JSON.stringify(data.deligates);
            $ionicLoading.hide(); 
        }).error(function(data) {
            $ionicLoading.hide(); 
        });
        $scope.$broadcast('scroll.refreshComplete');
    }
    loadData();

    $ionicPopover.fromTemplateUrl('templates/popover.html', {
      scope: $scope,
    }).then(function(popover) {
      $scope.popover = popover;
    });

    $scope.doRefresh = function() {
        loadData();
    };
    
    var deligates = JSON.parse(window.localStorage['deligates']);
    console.log(deligates);
    $scope.deligates = deligates;
    $ionicLoading.hide();
    $scope.data = {
        "airlines": deligates,
        "search": ''
    };
  $scope.search = function() {
    DeligateDataService.searchDeligates($scope.data.search).then(
      function(matches) {
        $scope.deligates = matches;
      }
    )
  }

  $scope.addContact = function(deligateId) {

      $.each(deligates, function(index, el) {
        if(el.id == deligateId) {
          $scope.contactForm = {

            "displayName": el.firstName+' '+el.lastName,
            "name": {
                "givenName": el.firstName+' '+el.lastName,
                "formatted": el.firstName+' '+el.lastName
            },
            "nickname": null,
            "phoneNumbers": [
                {
                    "value": el.mobile,
                    "type": "mobile"
                }
            ],
            "emails": [
                {
                    "value": el.email,
                    "type": "home"
                }
            ],
            "addresses": [],
            "ims": null,
            "organizations": el.company,
            "birthday": null,
            "note": "",
            "photos": null,
            "categories": null,
            "urls": null
          }
        }
      });

      $cordovaContacts.save($scope.contactForm).then(function(result) {
          var alertPopup = $ionicPopup.alert({
           title: 'Success',
           template: 'We\'ve saved '+$scope.contactForm.displayName+'\'s number to your phone book.',
           buttons: [{
            text: 'Awesome',
            type: 'button-cyan'
            }] 
          });
      }, function(err) {
          var alertPopup = $ionicPopup.alert({
           title: 'Error',
           template: 'We could\'nt save the number :(',
           buttons: [{
            text: 'Whatever',
            type: 'button-cyan'
            }] 
          });
      });
  };

}]);