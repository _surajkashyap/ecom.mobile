angular.module('push.controllers', ['ionic', 'ngCordova'])

.controller('PushCtrl', function($scope, $ionicModal, $timeout, $ionicLoading, $http, $ionicPopup, ionicToast, $rootScope, $ionicUser, $ionicPush, $cordovaLocalNotification, $ionicPlatform, $cordovaPush) {

    var deviceToken = window.localStorage['device_token'];
    var userId = window.localStorage['userId'];
    var user = $ionicUser.get();

    if (!user.user_id) {
        user.user_id = $ionicUser.generateGUID();
    };

    // Metadata
    angular.extend(user, {
        name: 'Suraj',
        bio: 'Developer of M-Commerce',
        device_token : window.localStorage['device_token'],
        platform : window.localStorage['platform']
    });

    // Identify your user with the Ionic User Service
    $ionicUser.identify(user).then(function() {
        $scope.identified = true;
        $scope.userId = user.user_id;
        window.localStorage['userId'] = user.user_id;
        console.log('Identified user ' + user.name + '\n ID ' + user.user_id);
    });

    if (deviceToken == null) {
        $rootScope.$on('$cordovaPush:tokenReceived', function(event, data) {
            window.localStorage['device_token'] = data.token;
            window.localStorage['platform'] = data.platform;
            console.log(data.token);
            $scope.token = data.token;
            location.reload();
        });  
    }


    $ionicPush.register({
        canShowAlert: true, 
        canSetBadge: true, 
        canPlaySound: true, 
        canRunActionsOnWake: true,
        onNotification: function(notification) {
            console.log(notification);
            // if (notification.payload.message) {
            //     $cordovaLocalNotification.add({
            //         id: "1234",
            //         message: notification.payload.message,
            //         title: notification.payload.title,
            //         autoCancel: true,
            //         sound: true
            //     }).then(function () {
            //         console.log("The notification has been set");
            //     });
            // }
            return true;
        }
    });
});