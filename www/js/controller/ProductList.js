angular.module('productlist.controllers', ['ionic', 'ngCordova'])

.controller('ProductListCtrl', function($scope, $ionicModal, $stateParams, $timeout, $http, $ionicLoading) {
	$ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});
	var categoryId = $stateParams.categoryId;
	$scope.sort = {};

	$ionicModal.fromTemplateUrl('templates/filter.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.modal = modal;
		
	});
	
	$scope.filter = function() {
		$scope.modal.show(200);
	};

	$scope.closeFilter = function() {
    	$scope.modal.hide(300);
  	};
	
	function loadListAction(page) {
		var req = {
	        method: 'GET', 
	        url: baseUrl+'api/rest/products?limit=5&page='+page,
	        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    };
	    $http(req).success(function(data) { 
	        $scope.products = data;
	        $ionicLoading.hide(); 
	    }).error(function(data) { 
	        $ionicLoading.hide(); 
	    });
	}

	function loadByCategory(categoryId) {
		var req = {
	        method: 'GET', 
	        url: baseUrl+'api/category/product?sort=&category_id='+categoryId,
	        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    };
	    $http(req).success(function(data) { 
	    	console.log(data);
	        $scope.products = data.products;
	        $ionicLoading.hide(); 
	    }).error(function(data) { 
	        $ionicLoading.hide(); 
	    });
	}

	$scope.submitFilter = function() {
		$ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});
		var req = {
	        method: 'GET', 
	        url: baseUrl+'api/category/product?category_id='+categoryId+'&sort='+$scope.sort.price,
	        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    };
	    $http(req).success(function(data) { 
	    	console.log(data);
	        $scope.products = data.products;
	        $ionicLoading.hide(); 
	        $scope.closeFilter();
	    }).error(function(data) { 
	        $ionicLoading.hide(); 
	    });
	}

	loadByCategory(categoryId);
});