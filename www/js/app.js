// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','ionic.service.core','ionic.service.push', 'starter.controllers',
  'ngCordova',
  'ionic-toast',
  'deligates.controllers', 
  'profile.controllers',
  'productlist.controllers', 
  'productdetail.controllers', 
  'category.controllers',
  'filter.controllers',
  'cart.controllers',
  'search.controllers',
  'checkout.controllers',
  'push.controllers',
  'success.controllers'])

.run(function($ionicPlatform, $ionicPopup) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }

    if(window.Connection) {
        if(navigator.connection.type == Connection.NONE) {
            $ionicPopup.confirm({
                title: "Internet Disconnected",
                content: "The internet is disconnected on your device."
            })
            .then(function(result) {
                if(!result) {
                    ionic.Platform.exitApp();
                }
            });
        }
    }

    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.overlaysWebView(true);
      StatusBar.backgroundColorByHexString("#358481");
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider, $ionicAppProvider) {
  
  $ionicAppProvider.identify({
    app_id: '301874e8',
    api_key: '67f5b7207007cedbb21e62826278f714f7f1c9d23edf41ac',
    dev_push: false
  });

  $stateProvider
    .state('app', {
    url: '/app',
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })


  .state('app.dashboard', {
    url: '/dashboard',
    views: {
      'menuContent': {
        templateUrl: 'templates/dashboard.html',
        controller: 'DashboardCtrl'
      }
    }
  })

    .state('app.profile', {
    url: '/profile',
    views: {
      'menuContent': {
        templateUrl: 'templates/profile.html',
        controller: 'ProfileCtrl'
      }
    }
  })

  .state('app.products', {
    url: '/products/:categoryId',
    views: {
      'menuContent': {
        templateUrl: 'templates/products.html',
        controller: 'ProductListCtrl'
      }
    }
  })

  .state('app.productDetail', {
    url: '/product-detail/:productId',
    views: {
      'menuContent': {
        templateUrl: 'templates/productDetail.html',
        controller: 'ProductDetailCtrl'
      }
    }
  })

  .state('app.cart', {
    url: '/cart',
    views: {
      'menuContent': {
        templateUrl: 'templates/cart.html',
        controller: 'CartCtrl'
      }
    }
  })

  .state('app.checkout', {
    url: '/checkout',
    views: {
      'menuContent': {
        templateUrl: 'templates/checkout.html',
        controller: 'CheckoutCtrl'
      }
    }
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html',
        controller: 'SearchCtrl'
      }
    }
  })

  .state('app.success', {
    url: '/success/:order_increment_id',
    views: {
      'menuContent': {
        templateUrl: 'templates/success.html',
        controller: 'SuccessCtrl'
      }
    }
  })

  .state('app.deligates', {
    url: '/deligates',
    views: {
      'menuContent': {
        templateUrl: 'templates/deligates.html',
        controller: 'DeligatesCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('app/dashboard');
      $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.withCredentials = false;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
});
